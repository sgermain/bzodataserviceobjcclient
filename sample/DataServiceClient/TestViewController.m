//
//  TestViewController.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "TestViewController.h"
#import "BZODataRequestQueue.h"
#import "BZODataObjectMapper.h"
#import "User.h"
#import "Transaction.h"
#import "DataServiceClientAppDelegate.h"

@interface TestViewController()

@property (nonatomic, retain) BZODataRequest *odata;
@property (nonatomic, retain) BZODataRequestQueue *odataQueue;

@end

@implementation TestViewController

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
	
    [super dealloc];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Memory Management 

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.odata = [[BZODataRequest alloc]init];
	
	self.odata.delegate = self;
	
	self.odataQueue = [BZODataRequestQueue new];
}

- (void)viewDidUnload {
	
	self.odata = nil;
	self.odataQueue = nil;
	
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)testTouched:(id)sender {
	
	[self.odata submitGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users()"];
}

- (IBAction)testPostTouched:(id)sender {
	
	NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
	[params setValue:@"NewUser" forKey:@"UserName"];
	[params setValue:@"1252355" forKey:@"AccessCode"];
	[params setValue:@"83484" forKey:@"RequestCode"];
	
	[self.odata submitPOSTRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users" httpBodyInfo:params];
}

- (IBAction)testGETOpTouched:(id)sender {
	
	BZODataRequest *request = [BZODataRequest operationForGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users()"];
	request.delegate = self;
	
	[self.odataQueue addOperation:request];
}

- (IBAction)testPOSTOPTouched:(id)sender {
	
NSMutableDictionary *params = [NSMutableDictionary dictionary];

[params setValue:@"NewUserFromOperation" forKey:@"UserName"];
[params setValue:@"1252355" forKey:@"AccessCode"];
[params setValue:@"83484" forKey:@"RequestCode"];

BZODataRequest *request = [BZODataRequest operationForPOSTRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users" httpBodyInfo:params];

request.delegate = self;

[self.odataQueue addOperation:request];
}

- (IBAction)testGETBlockTouched:(id)sender {
    
    BZODataRequest *request = [BZODataRequest new];
    
    [request submitGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users()" 
          withCompletionBlock:^(BZODataRequest *request, NSDictionary *response) {
              
              NSLog(@"Submit GET WithCompletedBlock: %@", response);
          } 
              withFailedBlock:^(BZODataRequest *request, NSError *error) {
                  
                  NSLog(@"Submit GET WithFailedBlock: %@", error);
              }];
}

- (IBAction)testMapperTouched:(id)sender {
    
    BZODataRequest *request = [BZODataRequest new];
    
    [request submitGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Users()$filter=UserId eq 1" 
          withCompletionBlock:^(BZODataRequest *request, NSDictionary *response) {
              
              BZODataObjectMapper *mapper = [BZODataObjectMapper new];
              
              NSArray *entities = [mapper extractEntitiesFromJSONDictionary:response];
              
              for (NSDictionary *entity in entities) {
                  
                  DataServiceClientAppDelegate *appDelegate = (DataServiceClientAppDelegate*)[UIApplication sharedApplication].delegate;
                  
                  NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
                  
                  User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];
                  
                  [mapper populateObject:user withJSONDictionary:entity];
                  
                  [appDelegate saveContext];
                  
                  [request release];
              }
              
          } 
              withFailedBlock:^(BZODataRequest *request, NSError *error) {
                  
                  NSLog(@"Submit GET WithFailedBlock: %@", error);
                  [request release];
              }];
}

- (IBAction)addTransTouched:(id)sender {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
	[params setValue:@"1" forKey:@"TransactionTypeID"];

	[params setValue: @"12.75" forKey:@"Amount"];
	[params setValue:[NSDate date] forKey:@"TransactionDate"];
	
	[self.odata submitPOSTRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Transactions" httpBodyInfo:params];
}

- (IBAction)getTransTouched:(id)sender {
    
    BZODataRequest *request = [BZODataRequest new];
    
    [request submitGETRequest:@"http://myallowance-services.apphb.com/DataServices.svc/Transactions(1)" 
          withCompletionBlock:^(BZODataRequest *request, NSDictionary *response) {
              
              NSDictionary *d = [response objectForKey:@"d"];
              
              NSLog(@"JSON Response: %@", d);
              
              BZODataObjectMapper *mapper = [BZODataObjectMapper new];
              
              DataServiceClientAppDelegate *appDelegate = (DataServiceClientAppDelegate*)[UIApplication sharedApplication].delegate;
              
              NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;
              
              Transaction *transaction = [NSEntityDescription insertNewObjectForEntityForName:@"Transaction" inManagedObjectContext:managedObjectContext];
              
              [mapper populateObject:transaction withJSONDictionary:d];
              
              NSLog(@"Transaction:%@", transaction);
          } 
              withFailedBlock:^(BZODataRequest *request, NSError *error) {
                  
                  NSLog(@"Submit GET WithFailedBlock: %@", error);
              }];

}

- (IBAction)testMERGETouched:(id)sender {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
	
    [params setValue:@"1" forKey:@"TransactionID"];
	[params setValue:[NSDate date] forKey:@"TransactionDate"];
	
	[self.odata submitMERGERequest:@"http://myallowance-services.apphb.com/DataServices.svc/Transactions(1)" httpBodyInfo:params];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - ODataRequestDelegate 

- (void)request:(BZODataRequest *)request didReceiveResponse:(NSDictionary *)response {
	
	NSLog(@"JSON Response: %@", response);
    [request release];
}

- (void)request:(BZODataRequest *)request didFailWithError:(NSError *)error {   

}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Properties

@synthesize odata;
@synthesize odataQueue;


@end
